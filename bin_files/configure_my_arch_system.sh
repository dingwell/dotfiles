#!/bin/bash
#
# This script will change some configuration files on a fresh install of arch

generate_locale(){
  # Give locale as argument
  # LOCALE should have this format: "sv_SE.UTF-8 UTF-8"

  GEN_FILE=/etc/locale.gen
  LOCALE=$1

  echo "Looking for: $LOCALE in $GEN_FILE"
  if grep "$LOCALE" "$GEN_FILE">/dev/null; then
    echo "'$LOCALE' is already in '$GEN_FILE' -- skipping"
  else
    echo "'$LOCALE' is not in '$GEN_FILE' -- appending"

    echo "Backing up old '$GEN_FILE':"
    sudo cp "$GEN_FILE" "${GEN_FILE}_$(date '+%Y-%m-%d_%H:%M:%S')"

    echo "Appending '$LOCALE' to '$GEN_FILE'"
    echo "$LOCALE" | sudo tee --append /etc/locale.gen > /dev/null

    echo "Generating new locale"
    sudo locale-gen
  fi

}

configure_locales(){
  LOCFILE=$HOME/.config/locale.conf
  echo 'export LC_ALL=""'                     >  "$LOCFILE"
  echo 'export LANG="en_US.utf8"'             >> "$LOCFILE"
  echo 'export LC_CTYPE="sv_SE.utf8"'         >> "$LOCFILE"
  echo 'export LC_NUMERIC="en_US.utf8"'       >> "$LOCFILE"
  echo 'export LC_TIME="sv_SE.utf8"'          >> "$LOCFILE"
  echo 'export LC_COLLATE="sv_SE.utf8"'       >> "$LOCFILE"
  echo 'export LC_MONETARY="sv_SE.utf8"'      >> "$LOCFILE"
  echo 'export LC_MESSAGES="en_US.utf8"'      >> "$LOCFILE"
  echo 'export LC_PAPER="sv_SE.utf8"'         >> "$LOCFILE"
  echo 'export LC_NAME="en_US.utf8"'          >> "$LOCFILE"
  echo 'export LC_ADDRESS="sv_SE.utf8"'       >> "$LOCFILE"
  echo 'export LC_TELEPHONE="sv_SE.utf8"'     >> "$LOCFILE"
  echo 'export LC_MEASUREMENT="sv_SE.utf8"'   >> "$LOCFILE"
  echo 'export LC_IDENTIFICATION="sv_SE.utf8"'>> "$LOCFILE"

  echo "Finished configuring locale"
  echo "Showing contents of $HOME/.locale :"
  cat $HOME/.locale |sed 's/^/ | /'

  echo "Setting keyboard layout for virtual console:"
  localectl set-keymap sv-latin1

}

echo "Adding locales to system"
generate_locale "sv_SE.UTF-8 UTF-8"

echo "Configuring custom locale settings for user: $(whoami)"
configure_locales
