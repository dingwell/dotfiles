#!/bin/bash

read -d '' DESKTOP << EOF
aspell-en aspell-sv 
awesome awesome-extra 
cups 
deja-dup 
gnome-keyring 
hunspell-en-us hunspell-en-ca hunspell-sv 
hyphen-en-us hyphen-en-ca hyphen-sv 
mythes-en-us mythes-sv 
nitrogen 
pavucontrol 
syncthing 
xcompmgr 
xserver-xephyr 
xtrlock
EOF

# xfce4-power-manager xfce4-settings xfdesktop 
#python2-gtkspellcheck 
#arc-theme arc-icons 
#gstreamer1.0-plugins-good gstreamer1.0-plugins-bad 
# gtkspell 
#xorg-xbacklight 
#xf86-input-wacom 
#xf86-synaptics-driver 
#telepathy-haze 
#python2-xdg qt5-base qt5-tools 
#rsync 
#python2-caja python2-gnomekeyring 
#sdl2_image sdl2_tff splix 

read -d '' APPLICATIONS << EOF
atril baobab bash-completion 
calibre 
cmake colordiff engrampa 
git 
gthumb 
inkscape 
keepassx 
libreoffice  libreoffice-texmaths 
rhythmbox sakura 
shotwell steam xournal 
EOF
# zimppa:noobslab/icons
#parole telegram 
#gst-libav \
#pluma \
#pybliographer \ NO LONGER IN REPOS!
#caja caja-open-terminal \
#claws-mail \
#geany \
#python2-poppler python2-reportlab 
# gourmet 
# libreoffice-write2latex

PPA='ppa:klaus-vormweg/awesome'
#ppa:noobslab/icons'

echo "Adding PPA:s prior to installation (requesting root access)"
for i in $PPA; do
  sudo add-apt-repository "$i"
done

echo "Installing desktop/system packages (requesting root access)"
sudo apt-get install $DESKTOP

echo "Installing other applications (requesting root access)"
sudo apt-get install $APPLICATIONS

echo "Installing custom packages using git"
set -e
sudo apt-get install autoconf
echo "Arc icon theme"
cd programming/git
git clone https://github.com/horst3180/arc-icon-theme --depth 1 && cd arc-icon-theme
./autogen.sh --prefix=/usr
sudo make install
cd ~/programming/git
echo "Install complete!"
