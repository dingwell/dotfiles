#!/bin/bash

DESKTOP=acpi adwaita-icon-theme \
        aspell-en aspell-sv \
        awesome \
        cups \
        deja-dup encfs glm \
        gnome-keyring gnome-themes-extra gvfs-gphoto2 \
        gvfs-mtp \
        gst-plugins-good gst-plugins-bad \
        gtkspell \
        hunspell-en_US hunspell-en_CA \
        hyphen-en keepassx2 luajit \
        mate-utils \
        midori mythes-en ninja \
        nitrogen openal pavucontrol \
        physfs \
        python2-caja python2-gnomekeyring \
        python2-gtkspellcheck \
        python2-xdg qt5-base qt5-tools \
        rsync \
        sdl2_image sdl2_tff splix \
        syncthing \
        telepathy-haze \
        util-linux \
        vicious xcompmgr \
        xf86-input-wacom \
        xf86-synaptics-driver \
        xfce4-power-manager xfce4-settings xfdesktop \
        xorg-server-xephyr xorg-xbacklight \
        xtrlock


APPLICATIONS=atril baobab bash-completion \
        caja caja-open-terminal \
        calibre claws-mail \
        cmake colordiff engrampa \
        geany git \
        gourmet python2-poppler python2-reportlab \
        gthumb \
        inkscape kepassx2 libreoffice-extensions-texmaths \
        libreoffice-extensions-writer2latex libreoffice-still \
        parole gst-libav \
        pluma \
        rhythmbox sakura \
        shotwell telegram xournal \
        zim

# NOTE: order matters for AUR_PACKS!
AUR_PACKS=adwaita-qt4 adwaita-qt5 arc-icon-theme \
        libgee06 gnome-encfs-manager-bzr \
        recode-pybliographer python-bibtex pybliographer \
        hunspell-sv hyphen-sv \
        mythes-sv xinit-xsession \
        solarus-git \
        zsdx-git

AUR_PATH="$HOME/source/aur"

# MISSING (not yet found) APPS:
# pybliographic screenruler xephyr XFCE

install_AUR(){
  # Takes 1 argument (package name)
  if [[ ! -d $AUR_PATH ]]; then
    mkdir -p $AUR_PATH
  fi

  export PACK_NAME=$1

  aur_error(){
    echo "installation of '$PACK_NAME' failed" 1>&2
  }

  echo "Fetching AUR-package '$PACK_NAME'"
  git clone https://aur.archlinux.org/${PACK_NAME}.git || {
    aur_error
    return 1
  }
 
  cd "$AUR_PATH/$PACK_NAME"

  echo "Building AUR-package '$PACK_NAME'"
  makepkg -s || {
    aur_error
    return 2
  }

  echo "Installing AUR-package '$PACK_NAME' (requesting root access)"
  pacman -U ${PACK_NAME}.tar.xz || {
    aur_error
    return 3
  }

  #echo "Cleaning up build-directory for AUR-package '$PACK_NAME'"
  #makepkg -c

  echo "Finished installing AUR-package '$PACK_NAME'"
}

setup_fstrim_service(){
  # Enables periodic fstrim on all mounted drives supporting with the
  # discard option. (requires util-linux package)
  read -p "Enable periodic fstrim (recommended for SSDs)" -n 1 -r
  echo    # (optional) move to a new line
  if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo "(Requesting root access for enabling fstrim)"
    sudo systemctl enable fstrim.timer
  fi
}


echo "Installing desktop/system packages (requesting root access)"
sudo pacman -S $DESKTOP

echo "Installing other applications (requesting root access)"
pacman -S $APPLICATIONS

echo "Installing AUR packages (one by one)"
for i in $AUR_PACKS; do
  install_AUR $i
done
