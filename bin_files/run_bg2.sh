#!/bin/bash

set -e

#True parent directory of the BG game directories:
#BG_CI_STASH=/mnt/USER-FILES/Games/bg-stash
BG_CI_STASH=~/.local/share/bg2-stash

# Mount point for the case-insensitive (virtual) file system:
#BG_CI_MOUNT=/mnt/USER-FILES/Games/bg-mount-point/
BG2_CI_MOUNT="/home/adam/.local/share/Baldur's Gate II - Enhanced Edition"
MOUNT_CHECK="$BG2_CI_MOUNT"
#MOUNT_CHECK="/home/adam/.local/share/Baldur"

# Name of Game directory:
#GAME_DIR="Baldur's Gate II Enhanced Edition"

# Path to Steam's library files:
#STEAM_LIBS=~/.steam/debian-installation/ubuntu12_32/steam-runtime/lib/x86_64-linux-gnu/
STEAM_LIBS=~/.steam/debian-installation/ubuntu12_32/steam-runtime/lib/x86_64-linux-gnu/:~/.steam/debian-installation/ubuntu12_32/steam-runtime/usr/lib/x86_64-linux-gnu/

echo "Checking if the case-insensitive game directory is mounted:"
if cat /proc/mounts|sed 's/\\040/ /g' |grep "$MOUNT_CHECK"; then
  echo "Mounted"
else
  echo "Not mounted, mounting"
  #ciopfs "$BG_CI_STASH" "$BG_CI_MOUNT"
  ciopfs "$BG_CI_STASH" "$BG2_CI_MOUNT"
  echo "Mounted"
fi

export LD_LIBRARY_PATH="$STEAM_LIBS":"$LD_LIBRARY_PATH"

# Enter Game dir:
#cd "$BG_CI_MOUNT/$GAME_DIR"
cd "$BG2_CI_MOUNT"
# Launch game:
echo "LD_LIBRARY_PATH="
echo "$LD_LIBRARY_PATH"
ldd BaldursGateII64
exec "./BaldursGateII64"
