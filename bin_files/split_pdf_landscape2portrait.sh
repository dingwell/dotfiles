#!/bin/bash
# Usage:
#   give a single file as argument, it is assumed that this file has
#   the following page format
#   (double portrait pages stored on a landscape page).
#
#   PAGE 1:        PAGE 2:
#   +-----+-----+  +-----+-----+
#   |     |     |  |     |     |
pdftk $(cat fl)  cat output tmp.pdf#   |  1  |  2  |  |  3  |  4  |
#   |     |     |  |     |     |
#   +-----+-----+  +-----+-----+
#
#   This script will produce a file named [inputfile]-portrait.pdf
#   The created file will have the following page format:
#
#   PAGE 1   PAGE 2   PAGE 3   PAGE 4
#   +-----+  +-----+  +-----+  +-----+
#   |     |  |     |  |     |  |     |  
#   |  1  |  |  2  |  |  3  |  |  4  |
#   |     |  |     |  |     |  |     |  
#   +-----+  +-----+  +-----+  +-----+
#   
# This script is based on the most basic example at:
# https://unix.stackexchange.com/questions/405610/how-can-i-split-each-pdf-page-into-two-pages-using-the-command-line
# Together with:
# /home/adam/Documents/Viktigt/utbildning/VÄP/termin3/litteratur/tmp/assymetric_split.sh
# A more general method could be made...

FILE=$1
echo "Input file is '$FILE'"

echo "Using the following binaries:"

which pdftk || {
  echo "pdftk is not installed on system, exiting."
  exit 1
}

which gs || {
  echo "gs is not installed on system, exiting."
  exit 2
}

which bc || {
  echo "bc is not installed on system, exiting."
  exit 3
}

split_landscape_pages(){
  # Reads data from doc_data.txt file (from: pdftk burst command)
  # Uses it to split single-page landscape documents named pg_[0-9]*.pdf
  # into twice as many, half as big single-page portrait documents.
  # The output files will be named:
  #   left_pg[0-9]*.pdf and right_pg[0-9]*.pdf
  pw=$(cat doc_data.txt  | grep PageMediaDimensions | head -1 | awk '{print $2}')
  ph=$(cat doc_data.txt  | grep PageMediaDimensions | head -1 | awk '{print $3}')
  #w2=$(( pw / 2 ))
  w2=$( bc <<< "scale=3; $pw/2")
  w2px=$( bc <<< "$w2*10/1" )
  hpx=$( bc <<< "$ph*10/1" )
  #echo "w2,w2px,hpx = $w2, $w2px, $hpx"
  # Clear logfile:
  > log.txt
  # Split files:
  for f in  pg_[0-9]*.pdf ; do
    echo "Splitting: '$f'"
    lf=left_$f
    rf=right_$f
    gs -o ${lf} -sDEVICE=pdfwrite -g${w2px}x${hpx} -c "<</PageOffset [0 0]>> setpagedevice" -f ${f} &>> log.txt
    gs -o ${rf} -sDEVICE=pdfwrite -g${w2px}x${hpx} -c "<</PageOffset [-${w2} 0]>> setpagedevice" -f ${f} &>> log.txt
done
}

merge_lr(){
  # Will merge all files named [lr].*_[0-9]*.pdf into a single file
  # e.g. files left_1.pdf right_1.pdf left_2.pdf right_2.pdf
  # will be merge into a singel pdf -- in that order

  ls -1 [lr]*_[0-9]*pdf | sort -n -k3 -t_ > fl
  pdftk $(cat fl)  cat output tmp.pdf
}

# Set script to exit at first failure from here on:

set -e

echo "Splitting document into one file per page."
pdftk "$FILE" burst

# We now have files named pg_NNNN.pdf and a an overview (doc_data.txt)
# Split each landscape document into two portrait documents:
echo "Splitting landscape pages into left and right portrait pages."
split_landscape_pages

# left pages are now name lNNNN and right pages are named rNNNN
# merge left and right pages into a single document:
echo "Re-merging pages into portrait pages pdf."
merge_lr

# The merged document is now named "tmp.pdf" rename it:
OUTFILE=$( echo "$FILE"|sed -r 's/.pdf$/-portrait.pdf/')
mv tmp.pdf "$OUTFILE"

#exit  #debug
echo "Cleaning up"
rm left_pg_[0-9]*[0-9].pdf
rm right_pg_[0-9]*[0-9].pdf
rm pg_[0-9]*[0-9].pdf
rm log.txt
rm fl
rm doc_data.txt
echo "Script exited without errors."


